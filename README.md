# timer
A cubing timer   
timer is a cubing timer made by me.         
Keybindings:   
0-9: Change session   
d: Delete last timer   
r: Delete all times in session (reset)         
Configuration:   
```
0:title   
1:title   
...   
...   
```

0-9: session titles   
Remember: NO SPACE AFTER COLON

## Changelog
1.0.0   
* First git version

1.0.1      
* Fixed title; had unused variables

1.0.2      
* Added custom colors
