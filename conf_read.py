import json

def read_config(obj):
    """obj: file object of config file"""
    lines = json.load(obj)
    names = ['','','','','','','','','','']
    for line in lines.keys():
        try:
            names[int(line)] = lines[line]
        except:
            pass
    return [names, lines["bg_color"], lines["fg_color"], lines["scramble_length"]]
